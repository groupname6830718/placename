/** @type {import('tailwindcss').Config} */
// tailwind.config.js
const plugin = require('tailwindcss/plugin');

module.exports = {
  mode: 'jit',
  content: [
    './app/**/*.{js,ts,jsx,tsx,mdx}',
    './_components/**/*.{js,ts,jsx,tsx,mdx}',
  ],

  theme: {
    extend: {
      screens: {
        'x-sm': '413px',
      },
      zIndex: {
        '-50': '-50 !important',
      },
    },
  },
  plugins: [
    plugin(function ({ addVariant }) {
      addVariant('short', '@media (max-height: 430px)');
      addVariant('tall', '@media (min-height: 431px)');
    }),
  ],
};
