import localFont from 'next/font/local';
import './globals.css';
import AppContextProvider from '@/app/_components/app-context-provider';
import LayoutSquare from '@/app/_components/layout-square';
import Script from 'next/script';

export const metadata = {
  title: process.env.NEXT_PUBLIC_PLACE,
  description: process.env.NEXT_PUBLIC_PLACE,
  robots: {
    index: false,
    follow: true,
    nocache: true,
    googleBot: {
      index: false,
      follow: true,
    },
  },
};

const SFMonoRegular = localFont({
  src: 'SF-Mono-Regular.otf',
  variable: '--SFMono-Regular',
  display: 'swap',
});

export default async function RootLayout({ children }) {
  const encodedPlace = encodeURIComponent(process.env.NEXT_PUBLIC_PLACE);

  const initialPlaceData = await fetch(
    `https://www.units-server.com/api/places/${encodedPlace}`,
    { next: { tags: ['place-data'] } }
  ).then((x) => x.json());

  const initialActionablePayments = await fetch(
    `https://www.units-server.com/api/payments?place=${encodedPlace}&status=requires_payment_method,requires_action&keys=name`,
    { next: { tags: ['payments'] } }
  ).then((x) => x.json());

  Object.keys(initialActionablePayments).forEach((key) => {
    initialActionablePayments[key].name = initialActionablePayments[
      key
    ].name.replace(/\d+/g, '');
  });

  return (
    <html>
      <body
        className={
          `bg-black ${SFMonoRegular.className} ` +
          (initialPlaceData.name.length >= 17
            ? 'text-3xl x-sm:text-4xl short:sm:text-2xl '
            : 'text-4xl short:sm:text-3xl')
        }
      >
        <div id='map-container' className='fixed w-[100%] h-[100%]'>
          <Script
            //  strategy={"beforeInteractive"}
            src='/map.js'
          />
        </div>
        <LayoutSquare background />
        <AppContextProvider
          initialPlaceData={initialPlaceData}
          initialActionablePayments={initialActionablePayments}
        >
          {children}
        </AppContextProvider>
      </body>
    </html>
  );
}
