import Lease from '@/app/_components/vacant/lease';
import RootPage from '@/app/page.js';

export default function Page() {
  return (
    <>
      <RootPage></RootPage>
      <Lease></Lease>
    </>
  );
}
