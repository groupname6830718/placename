import Payments from '@/app/_components/payments';
import toDataGridRows from '@/app/_lib/toDataGridRows';

export default async function Page() {
  const initialPayments = await fetch(
    'https://www.units-server.com/api/payments?place=' +
      encodeURIComponent(process.env.NEXT_PUBLIC_PLACE),
    { next: { tags: ['payments'] } }
  ).then((x) => x.json());

  return <Payments initialRows={toDataGridRows(initialPayments)} />;
}
