import { NextResponse } from 'next/server';
import { revalidateTag } from 'next/cache';
export const runtime = 'edge';
export async function GET() {
  revalidateTag('payments');
  return NextResponse.json({ revalidated: true, now: Date.now() });
}
