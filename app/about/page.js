import LayoutSquare from '@/app/_components/layout-square';
import Link from 'next/link';
import { RedirectDemoButton } from '@/app/_components/vacant/redirect-demo-button';

export default async function Page({ isSuccess }) {
  const initialPlaceData = await fetch(
    'https://units-server.com/api/places/' +
      encodeURIComponent(process.env.NEXT_PUBLIC_PLACE),
    { next: { tags: ['place-data'] } }
  ).then((x) => x.json());

  return (
    <LayoutSquare
      header={
        <div className={'z-50'}>
          <Link href={isSuccess ? '/success' : '/'}>{`<<`}</Link>
        </div>
      }
    >
      <div
        className={
          'text-white text-lg leading-snug text-center leading-relaxed -mt-2.5 -z-50 sm:tall:backdrop-blur-[1px]'
        }
      >
        {`alternative rental portals are not something that I would want to use, so I built one. if you decide to rent ${initialPlaceData['name']}, you can expect a wonderful digital experience. `}
      </div>

      <RedirectDemoButton sx={{ marginTop: '-.75rem' }} />
    </LayoutSquare>
  );
}
