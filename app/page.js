import Link from 'next/link';
import LayoutSquare from '@/app/_components/layout-square';
import ActionablePayments from '@/app/_components/actionable-payments';
//
export default async function Page({ isSuccess }) {
  const { applications_open, name } = await fetch(
    'https://units-server.com/api/places/' +
      encodeURIComponent(process.env.NEXT_PUBLIC_PLACE),
    { next: { tags: ['place-data'] } }
  ).then((x) => x.json());

  const links = applications_open
    ? [
        {
          href: isSuccess ? '/success/application' : '/application',
          text: 'application',
        },
        {
          href: isSuccess ? '/success/lease' : '/lease',
          text: 'lease',
        },
        { href: isSuccess ? '/success/about' : '/about', text: '...' },
      ]
    : [
        { href: '/autopay', text: 'autopay' },
        { component: <ActionablePayments /> },
        { href: '/payments', text: '...' },
      ];

  const header = (
    <>
      {name}
      <br />
      {links.map(({ href, text, component }) => (
        <div key={href || 'component'}>
          {href && <Link href={href}>{text}</Link>}
          {component && component}
        </div>
      ))}
    </>
  );

  return <LayoutSquare header={header} />;
}
