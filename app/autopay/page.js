'use client';
import LayoutSquare from '@/app/_components/layout-square';
import Link from 'next/link';
import { useContext } from 'react';
import { PlaceDataContext } from '@/app/_components/app-context-provider';

export default function Page() {
  const placeData = useContext(PlaceDataContext);

  return (
    <LayoutSquare
      bouncer
      header={
        <Link href={'/'}>
          {`<< autopay: ${placeData['autopay'] ? 'on' : 'off'}`}
        </Link>
      }
    ></LayoutSquare>
  );
}
