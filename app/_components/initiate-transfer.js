import DialogContent from '@mui/material/DialogContent';
import Dialog from '@mui/material/Dialog';
import ThemeProvider from '@mui/material/styles/ThemeProvider';
import { Elements } from '@stripe/react-stripe-js';
import { loadStripe } from '@stripe/stripe-js';
import { usePathname, useRouter } from 'next/navigation';
import { useEffect } from 'react';
import { MUIWhiteBackgroundBlackText } from '@/app/_components/mui-themes';
import { CheckoutForm } from '@/app/_components/checkout-form';

const stripePromise = loadStripe(
  'pk_live_51MqnIsKaqC0LBJQA7htbxiB9Ao1Bhox8eUM2ErMEjNkdu7jYeGsUAHFi1lL1wAkSccOiyqg29qeVIzGixijSE1yQ00lXP6w1Nr'
);

export default function InitiateTransfer({ payment }) {
  const router = useRouter();

  useEffect(() => {
    router.prefetch('/');
  }, []);

  const appearance = {
    variables: {
      colorPrimary: '#000000',
      colorText: '#000000',
      colorDanger: '#000000',
      fontFamily:
        'SFMono-Regular, Consolas, Liberation Mono, Menlo, Courier, monospace',
      colorIcon: '#000000',
    },
  };

  console.log(payment);

  return (
    <ThemeProvider theme={MUIWhiteBackgroundBlackText}>
      <Dialog
        id={payment.name}
        className={
          usePathname().split('/').pop() !== payment.name
            ? 'invisible -z-50'
            : ''
        }
        open={true}
        onClose={() => router.push(`/`)}
        fullWidth
        maxWidth={false}
        scroll={'body'}
        PaperProps={{ style: { borderRadius: 0, maxWidth: '400px' } }}
        hideBackdrop
      >
        <DialogContent>
          {payment.client_secret && (
            <Elements
              options={{ clientSecret: payment.client_secret, appearance }}
              stripe={stripePromise}
            >
              <CheckoutForm payment={payment} />
            </Elements>
          )}
        </DialogContent>
      </Dialog>
    </ThemeProvider>
  );
}
