import { createTheme } from '@mui/material';

export const MUIWhiteBackgroundBlackText = createTheme({
  palette: {
    primary: { main: 'rgba(0,0,0,1) !important' },
  },
  typography: { fontFamily: ['var(--SFMono-Regular)'] },
  components: {
    MuiInputLabel: {
      styleOverrides: { root: { color: 'black !important' } },
    },
    MuiButton: {
      styleOverrides: {
        root: {
          textTransform: 'none',
        },
      },
    },
    MuiInput: {
      styleOverrides: {
        underline: {
          '&:hover:not(.Mui-disabled):before': {
            borderBottomColor: 'black',
          },
        },
      },
    },
  },
});
export const MUIAppDefaultTheme = createTheme({
  palette: {
    primary: {
      main: '#ffffff !important',
    },
    secondary: {
      main: '#000000 !important',
    },
    text: {
      primary: '#ffffff !important',
      secondary: '#ffffff !important',
    },
  },
  typography: {
    fontFamily: 'var(--SFMono-Regular)',
  },
  components: {
    MuiButton: {
      styleOverrides: {
        root: {
          backgroundColor: '#ffffff !important',
          '&:hover': {
            backgroundColor: 'sm:rgba(200, 200, 200, 1)!important',
          },
        },
      },
    },
  },
});
