'use client';
import { useContext, useEffect, useMemo, useState } from 'react';
import { useRouter } from 'next/navigation';
import {
  AuthenticatedUserContext,
  PlaceDataContext,
} from '@/app/_components/app-context-provider';
import Stack from '@mui/material/Stack';
import Button from '@mui/material/Button';
import Pagination from '@mui/material/Pagination';

export default function AuthenticatedContent({ children, paymentInfo }) {
  const authenticatedUser = useContext(AuthenticatedUserContext);

  if (authenticatedUser === 'loading') return null;

  return authenticatedUser ? children : <Login paymentInfo={paymentInfo} />;
}

export function Login({ paymentInfo }) {
  const { logins = [] } = useContext(PlaceDataContext);
  const [loginsPerPage, setLoginsPerPage] = useState(5);
  const [page, setPage] = useState(1);
  const router = useRouter();

  useEffect(() => {
    const newLoginsPerPage = getLoginsPerPage();
    setLoginsPerPage(newLoginsPerPage);

    window.addEventListener('resize', updateLoginsPerPage);
    return () => window.removeEventListener('resize', updateLoginsPerPage);
  }, []);

  const displayedLogins = useMemo(() => {
    const offset = (page - 1) * loginsPerPage;
    const end = offset + loginsPerPage - (paymentInfo && page === 1 ? 1 : 0);
    return logins.slice(offset, end);
  }, [logins, page, paymentInfo, loginsPerPage]);

  function getLoginsPerPage() {
    if (typeof window !== 'undefined') {
      return window.innerWidth <= 640 ? 6 : window.innerHeight <= 430 ? 5 : 7;
    }
    return 5;
  }

  function updateLoginsPerPage() {
    const newLoginsPerPage = getLoginsPerPage();
    setLoginsPerPage(newLoginsPerPage);
  }
  return (
    <div style={{ position: 'relative', height: '100%' }}>
      <Stack
        spacing={1.25}
        sx={{
          width: '95%',
          margin: '0 auto',
          marginTop: '0.2rem',
          opacity: '76%',
        }}
      >
        {displayedLogins.map((login, index) => (
          <Button
            key={index}
            onClick={() => console.log(5)}
            variant='contained'
            disableElevation
            color='primary'
            size='small'
          >
            {login.value}
          </Button>
        ))}
        {/*{paymentInfo && page === 1 && (*/}
        {/*  <BypassButton*/}
        {/*    onClick={() => router.push(`/${paymentInfo.payment}/bypass`)}*/}
        {/*  >*/}
        {/*    bypass*/}
        {/*  </BypassButton>*/}
        {/*)}*/}
      </Stack>
      <Pagination
        count={Math.ceil(logins.length / loginsPerPage)}
        page={page}
        onChange={(event, value) => setPage(value)}
        sx={{
          display: 'flex',
          justifyContent: 'center',
          position: 'fixed',
          bottom: '0',
          left: '50%',
          width: '100%',
          transform: 'translateX(-50%)',
        }}
      />
    </div>
  );
}
