import AuthenticatedContent from '@/app/_components/authenticated-content';

export default function LayoutSquare({
  background,
  header,
  children,
  bouncer,
  paymentInfo,
}) {
  const bgElement = background && (
    <div className='absolute w-full h-full bg-slate-400 opacity-40 rounded-xl shadow-sm shadow-slate-300' />
  );

  const content = bouncer ? (
    <AuthenticatedContent paymentInfo={paymentInfo}>
      {children}
    </AuthenticatedContent>
  ) : (
    children
  );

  return (
    <div
      className={
        ' fixed w-11/12 h-80 bottom-0 inset-x-0 mx-auto z-10 flex flex-col sm:short:w-72 sm:short:h-72 sm:w-96 sm:h-96 sm:top-1/2 sm:right-1/4 sm:translate-x-1/2 sm:-translate-y-1/2 sm:bottom-auto sm:inset-x-auto backdrop-blur-[0.75px]'
      }
    >
      {bgElement}
      <div className='relative w-full h-full z-20'>
        <div className='text-white leading-snug text-center flex flex-col items-center'>
          {header}
        </div>
        {content}
      </div>
    </div>
  );
}
