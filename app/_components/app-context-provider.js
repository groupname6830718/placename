'use client';
import { createContext, useEffect, useState, Suspense, lazy } from 'react';
import { ThemeProvider } from '@mui/material';

const InitiateTransfer = lazy(() => import('@/app/_components/initiate-transfer'));
import { MUIAppDefaultTheme } from '@/app/_components/mui-themes';

export const PlaceDataContext = createContext(null);
export const ActionablePaymentsContext = createContext(null);
export const AuthenticatedUserContext = createContext(null);
export const DynamicFirebaseContext = createContext('firebase');

export const firebaseConfig = {
  apiKey: 'AIzaSyCqK0UKHjqvttmZzIatEjLjfjX8P2gg1tE',
  authDomain: 'projectname-xxx.firebaseapp.com',
  projectId: 'projectname-xxx',
  storageBucket: 'projectname-xxx.appspot.com',
  messagingSenderId: '323589431735',
  appId: '1:323589431735:web:bdea4c7439419dbc578832',
};

export default function AppContextProvider({
  initialPlaceData,
  initialActionablePayments,
  children,
}) {
  const [placeData, setPlaceData] = useState(initialPlaceData);
  const [actionablePayments, setActionablePayments] = useState(
    initialActionablePayments
  );
  const [authenticatedUser, setAuthenticatedUser] = useState(null);
  const [dynamicFirebase, setDynamicFirebase] = useState(null);

  useEffect(() => {
    const initializeFirebase = async () => {
      const { initializeApp } = await import('firebase/app');
      const [
        firestoreModule,
        //      , storageModule, authModule
      ] = await Promise.all([
        import('firebase/firestore'),
        //        ,
        //        import('firebase/storage'),
        //        import('firebase/auth'),
      ]);

      const firebaseApp = initializeApp(firebaseConfig);
      const firestore = firestoreModule.getFirestore(firebaseApp);
      //      const storage = storageModule.getStorage(firebaseApp);
      //      const auth = authModule.getAuth(firebaseApp);

      const firebase = { ...firestoreModule, firestore };
      setDynamicFirebase(firebase);

      const placeDataListener = firestoreModule.onSnapshot(
        firestoreModule.doc(firestore, 'places', process.env.NEXT_PUBLIC_PLACE),
        (snapshot) => setPlaceData(snapshot.data())
      );

      const actionablePaymentsListener = firestoreModule.onSnapshot(
        firestoreModule.query(
          firestoreModule.collection(firestore, 'payments'),
          firestoreModule.where('status', "in", ['requires_payment_method', 'requires_action']),
          firestoreModule.where('place', '==', process.env.NEXT_PUBLIC_PLACE),
          firestoreModule.orderBy('created', 'desc')
        ),
        (snapshot) => {
          const updatedPayments = {};
          snapshot.docs.forEach((document) => {
            const paymentData = {
              ...document.data(),
              payment: document.id,
              name: document.data().name.replace(/\d+/g, ''),
            };
            updatedPayments[paymentData.name] = paymentData;
          });

          setActionablePayments(updatedPayments);
        }
      );

      return () => {
        placeDataListener();
        actionablePaymentsListener();
      };
    };

    initializeFirebase();
  }, []);

  return (
    <DynamicFirebaseContext.Provider value={dynamicFirebase}>
      <PlaceDataContext.Provider value={placeData}>
        <ActionablePaymentsContext.Provider value={actionablePayments}>
          <AuthenticatedUserContext.Provider value={authenticatedUser}>
            <ThemeProvider theme={MUIAppDefaultTheme}>
              {children}
              <Suspense fallback={<div>Loading...</div>}>
                {Object.keys(actionablePayments).map((paymentKey) => (
                  <InitiateTransfer
                    key={paymentKey}
                    payment={actionablePayments[paymentKey]}
                  />
                ))}
              </Suspense>
            </ThemeProvider>
          </AuthenticatedUserContext.Provider>
        </ActionablePaymentsContext.Provider>
      </PlaceDataContext.Provider>
    </DynamicFirebaseContext.Provider>
  );
}
