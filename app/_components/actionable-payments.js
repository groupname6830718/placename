'use client';
import { useContext } from 'react';
import { ActionablePaymentsContext } from '@/app/_components/app-context-provider';
import Link from 'next/link';

export default function ActionablePayments() {
  const actionablePayments = useContext(ActionablePaymentsContext);
  const keys = Object.keys(actionablePayments);

  return (
    < >
      {keys.map((key) => (
        <Link key={key} href={`/${key}`}>
          {actionablePayments[key].name}<br/>
        </Link>
      ))}
    </>
  );
}
