'use client';
import { useContext, useEffect, useState } from 'react';
import Box from '@mui/material/Box';
import { createTheme, ThemeProvider } from '@mui/material';
import { DataGridPremium } from '@mui/x-data-grid-premium';
import Link from 'next/link';
import LayoutSquare from '@/app/_components/layout-square';
import toDataGridRows from '@/app/_lib/toDataGridRows';
import { toNumberForm } from '@/app/_lib/toNumberForm';
import { DynamicFirebaseContext } from '@/app/_components/app-context-provider';

export default function Payments({ initialRows }) {
  const [rows, setRows] = useState(initialRows);
  const firebase = useContext(DynamicFirebaseContext);

  useEffect(() => {
    const paymentsListener = () => {
      if (firebase) {
        const unsubscribe = firebase.onSnapshot(
          firebase.query(
            firebase.collection(firebase.firestore, 'payments'),
            firebase.where('place', '==', process.env.NEXT_PUBLIC_PLACE),
            firebase.orderBy('created', 'desc')
          ),
          (snapshot) => {
            const update = {};
            snapshot.forEach((doc) => (update[doc.id] = doc.data()));
            setRows(toDataGridRows(update));
          }
        );
        return () => unsubscribe();
      }
    };
    paymentsListener()
  }, [firebase]);

  return (
    <LayoutSquare
      header={
        <>
          <Link href={'/'}> &lt;&lt; </Link>{' '}
        </>
      }
    >
      <ThemeProvider theme={DataGridTheme}>
        <Box
          sx={{
            height: '100%',
            width: '100%',
            maxHeight: 'calc(100% - 2rem)',
          }}
        >
          <DataGridPremium
            disableRowSelectionOnClick
            hideFooter
            className=''
            density='compact'
            sx={{
              marginTop: '-1rem',
              borderStyle: 'none',
              height: '100%',
              width: '100%',
              '& .MuiDataGrid-columnHeaders, .MuiDataGrid-cell': {
                borderStyle: 'none',
              },
              '& .MuiButton-contained': {
                textTransform: 'lowercase',
              },
            }}
            columns={columns}
            rows={rows}
          />{' '}
        </Box>{' '}
      </ThemeProvider>{' '}
    </LayoutSquare>
  );
}
export const columns = [
  {
    field: 'name',
    headerAlign: 'center',
    align: 'center',
    flex: 1,
    valueGetter: (x) =>
      x.value.replaceAll(String(new Date().getFullYear()).slice(-2), ''),
  },
  {
    field: 'amount',
    flex: 1,
    headerAlign: 'center',
    type: 'number',
    align: 'center',
    valueGetter: (x) => toNumberForm(x.value / 100),
  },
  {
    field: 'status',
    flex: 1,
    headerAlign: 'center',
    align: 'center',
    valueGetter: (x) => {
      if (x.value === 'requires_payment_method' || x.value === "requires_action") {
        return 'unlocked';
      } else {
        return x.value;
      }
    },
  },
];
export const DataGridTheme = createTheme({
  palette: {
    primary: {
      main: '#ffffff',
    },
    text: {
      primary: '#ffffff',
      secondary: '#ffffff',
    },
    background: {
      paper: '#000000',
      default: '#000000',
    },
  },
  typography: {
    fontFamily: 'var(--font-SFMonoRegular)',
  },
});
