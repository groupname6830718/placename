'use client';
import { useMemo } from 'react';
import Box from '@mui/material/Box';
import IconButton from '@mui/material/IconButton';
import LinearProgress from '@mui/material/LinearProgress';
import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import ListItemSecondaryAction from '@mui/material/ListItemSecondaryAction';
import ListItemText from '@mui/material/ListItemText';
import Typography from '@mui/material/Typography';
import AddSharpIcon from '@mui/icons-material/AddSharp';
import RemoveSharpIcon from '@mui/icons-material/RemoveSharp';
import useCloudStorage from '@/app/_lib/useCloudStorage';

function ApplicationBlobUpload({ question, value, setValue }) {
  const {
    files,
    handleUpload,
    handleDelete,
    progress,
    uploading,
    setTotalBytes,
    totalBytes,
  } = useCloudStorage(value, setValue);

  const fileList = useMemo(
    () =>
      files.map((file) => {
        const filename = file.path.split('/').pop();
        return (
          <ListItem key={file.id}>
            <ListItemText
              primary={
                <a
                  href={file.url}
                  target='_blank'
                  rel='noopener noreferrer'
                  style={{ textDecoration: 'none', overflowWrap: 'break-word' }}
                >
                  {filename.replace(/_\d+$/, '')}
                </a>
              }
            />
            <ListItemSecondaryAction>
              <IconButton onClick={() => handleDelete(file.id)} edge='end'>
                <RemoveSharpIcon sx={{ color: 'rgb(27,27,28)' }} />
              </IconButton>
            </ListItemSecondaryAction>
          </ListItem>
        );
      }),
    [files, handleDelete]
  );

  const handleFileChange = async (e) => {
    const filesToUpload = Array.from(e.target.files);
    const newTotalBytes = filesToUpload.reduce(
      (acc, file) => acc + file.size,
      0
    );
    setTotalBytes((prevTotalBytes) => prevTotalBytes + newTotalBytes);

    await Promise.all(filesToUpload.map((file) => file && handleUpload(file)));
    e.target.value = null;
  };

  return (
    <>
      <Typography sx={{ mt: 4.5, textAlign: 'center' }}>{question}</Typography>
      <List sx={{ mt: -1 }}>{fileList}</List>
      {uploading && progress > 0 && (
        <Box sx={{ mt: 2 }}>
          <LinearProgress variant='determinate' value={progress} />
          <Typography variant='caption' align='center'>
            {Math.round((totalBytes * progress) / 100)} / {totalBytes} bytes (
            {Math.round(progress)}%)
          </Typography>
        </Box>
      )}

      <Box sx={{ display: 'flex', justifyContent: 'center' }}>
        <input
          type='file'
          multiple
          onChange={handleFileChange}
          style={{ display: 'none' }}
          id='file-input'
        />
        <label htmlFor='file-input'>
          <IconButton component='span'>
            <AddSharpIcon fontSize='default' sx={{ color: 'black' }} />
          </IconButton>
        </label>
      </Box>
    </>
  );
}

export default ApplicationBlobUpload;
