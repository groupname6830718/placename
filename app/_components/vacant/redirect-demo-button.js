'use client';
import Box from '@mui/material/Box';
import Button from '@mui/material/Button';

export function RedirectDemoButton(props) {
  return (
    <Box className={'mx-auto mt-2'} sx={{ width: '90%', opacity: '70%' }}>
      <Button
        {...props}
        fullWidth
        onClick={() => window.open('https://1910e14.com')}
        variant='contained'
        size='small'
        disableElevation
        color={'primary'}
      >
        {'demo'}
      </Button>
    </Box>
  );
}
