'use client';
import { useContext, useEffect } from 'react';
import { useRouter } from 'next/navigation';
import Button from '@mui/material/Button';
import Dialog from '@mui/material/Dialog';
import DialogContent from '@mui/material/DialogContent';
import ThemeProvider from '@mui/material/styles/ThemeProvider';
import Typography from '@mui/material/Typography';
import LayoutSquare from '@/app/_components/layout-square';
import ApplicationFreeResponse from '@/app/_components/vacant/application-free-response';
import ApplicationBlobUpload from '@/app/_components/vacant/application-blob-upload';
import ApplicationListUsers from '@/app/_components/vacant/application-list-users';
import { addDoc, collection, serverTimestamp } from 'firebase/firestore';
import { DynamicFirebaseContext } from '@/app/_components/app-context-provider';
import useLocalStorage from '@/app/_lib/useLocalStorage';
import { MUIWhiteBackgroundBlackText } from '@/app/_components/mui-themes';

export default function Application({ isSuccess }) {
  const router = useRouter();
  const firebase = useContext(DynamicFirebaseContext);

  useEffect(() => {
    router.prefetch(isSuccess ? '/success' : '/');
  }, []);

  const [freeResponse, setFreeResponse] = useLocalStorage('freeResponse', '');
  const [users, setUsers] = useLocalStorage('users', {
    usersCount: '',
    usersInfo: [''],
  });
  const [files, setFiles] = useLocalStorage('files', []);

  const onSubmit = async () => {
    try {
      await addDoc(collection(firebase.db, 'applications'), {
        questionData: freeResponse,
        usersData: users,
        documentsData: files,
        timestamp: serverTimestamp(),
        place: process.env.NEXT_PUBLIC_PLACE,
      });

      setFreeResponse('');
      setUsers({ usersCount: '', usersInfo: [''] });
      setFiles([]);
      router.push('/success');
    } catch (error) {
      console.error('Error submitting application: ', error);
      alert(`Error submitting application: ${error}`);
    }
  };

  return (
    <>
      <LayoutSquare>
        <ThemeProvider theme={MUIWhiteBackgroundBlackText}>
          <Dialog
            open={true}
            hideBackdrop
            onClose={() => router.push(isSuccess ? '/success' : '/')}
            fullWidth
            maxWidth={false}
            scroll={'body'}
            PaperProps={{ style: { borderRadius: 0, maxWidth: '400px' } }}
          >
            <DialogContent>
              <Typography className='text-center' variant='h6' component='h2'>
                application
              </Typography>

              <ApplicationFreeResponse
                question='describe your ideal living environment and the lifestyle you envision'
                value={freeResponse}
                setValue={setFreeResponse}
              />

              <ApplicationListUsers value={users} setValue={setUsers} />

              <ApplicationBlobUpload
                question='upload files showing income, any financial reserves, and ID'
                value={files}
                setValue={setFiles}
              />

              <Button
                fullWidth
                onClick={onSubmit}
                variant='contained'
                sx={{ mt: '15px' }}
                disableElevation
                color='primary'
              >
                apply
              </Button>
            </DialogContent>
          </Dialog>
        </ThemeProvider>
      </LayoutSquare>
    </>
  );
}
