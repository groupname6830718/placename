'use client';
import { useEffect, useState } from 'react';
import Box from '@mui/material/Box';
import TextField from '@mui/material/TextField';

export default function ApplicationListUsers({ value, setValue }) {
  const [state, setState] = useState(value);

  useEffect(() => {
    setValue(state);
  }, [state]);

  const handleUsersCountChange = (e) => {
    const value = e.target.value;
    if (value === '' || /^([0-9]|[1-9][0-9]|100)$/.test(value)) {
      const usersCount = value === '' ? '' : parseInt(value, 10);
      setState({ ...state, usersCount });
    }
  };

  const handleTextFieldChange = (e, index) => {
    const updatedData = [...state.usersInfo];
    updatedData[index] = e.target.value;
    setState({ ...state, usersInfo: updatedData });
  };

  const renderCount = Math.min(
    10,
    Math.max(1, parseInt(state.usersCount, 10) || 1)
  );
  const textFields = Array.from({ length: renderCount }, (_, i) => (
    <TextField
      key={i}
      sx={{ width: '100%', mt: i === 0 ? 0.25 : 1 }}
      variant='standard'
      label={`name, phone, email${i === 0 ? '' : ', relation'}`}
      value={state.usersInfo[i] || ''}
      onChange={(e) => handleTextFieldChange(e, i)}
    />
  ));

  return (
    <>
      <Box sx={{ display: 'flex', justifyContent: 'center', mt: 2 }}>
        <TextField
          sx={{ width: '50px' }}
          label='users'
          inputMode='numeric'
          variant='standard'
          value={state.usersCount}
          onChange={handleUsersCountChange}
          inputProps={{ style: { textAlign: 'center' } }}
        />
      </Box>
      {textFields}
    </>
  );
}
