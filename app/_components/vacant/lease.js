'use client';
import Dialog from '@mui/material/Dialog';
import { useRouter } from 'next/navigation';
import { ThemeProvider } from '@mui/material';
import Typography from '@mui/material/Typography';
import { MUIWhiteBackgroundBlackText } from '@/app/_components/mui-themes';

export default function Lease({ isSuccess }) {
  const router = useRouter();
  return (
    <>
      <ThemeProvider theme={MUIWhiteBackgroundBlackText}>
        <Dialog
          open={true}
          hideBackdrop
          onClose={() => router.push(isSuccess ? '/success' : '/')}
          fullWidth
          maxWidth={false}
          scroll={'body'}
          PaperProps={{ style: { borderRadius: 0, maxWidth: '400px' } }}
        >
          <Typography className='text-center' variant='h6' component='h2'>
            lease TBD
          </Typography>
        </Dialog>
      </ThemeProvider>
    </>
  );
}
