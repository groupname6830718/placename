'use client';
import Typography from '@mui/material/Typography';
import TextField from '@mui/material/TextField';

export default function ApplicationFreeResponse({ question, value, setValue }) {
  return (
    <div className='text-center'>
      <Typography sx={{ mt: 2 }}>{question}</Typography>
      <TextField
        sx={{ width: '100%', mt: 2 }}
        multiline
        rows={3}
        variant='standard'
        value={value}
        onChange={(e) => setValue(e.target.value)}
      />
    </div>
  );
}
