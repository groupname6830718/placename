import Application from '@/app/_components/vacant/application';
import RootPage from '@/app/page.js';
export default function Page({ isSuccess }) {
  return (
    <>
      <RootPage />
      <Application isSuccess={isSuccess} />
    </>
  );
}
