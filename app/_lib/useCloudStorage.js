import { useCallback, useContext, useState } from 'react';
import {
  deleteObject,
  getDownloadURL,
  ref,
  uploadBytesResumable,
} from 'firebase/storage';
import { DynamicFirebaseContext } from '@/app/_components/app-context-provider';

export const useCloudStorage = (storedValue, setValue) => {
  const [files, setFiles] = useState(storedValue);
  const [uploadProgress, setUploadProgress] = useState([]);
  const [uploading, setUploading] = useState(false);
  const [bytesTransferred, setBytesTransferred] = useState(0);
  const [totalBytes, setTotalBytes] = useState(0);
  const firebase = useContext(DynamicFirebaseContext);

  const updateProgress = (fileId, progress) => {
    setUploadProgress((prevProgress) => {
      const updatedProgress = prevProgress
        .filter((p) => p.id !== fileId)
        .concat({ id: fileId, progress });
      return updatedProgress;
    });
  };

  const handleUpload = useCallback(async (file) => {
    setUploading(true);
    const fileId = `${file.name}_${new Date().getTime()}`;
    const storageRef = ref(firebase.storage, `uploads/${fileId}`);

    const uploadTask = uploadBytesResumable(storageRef, file);
    uploadTask.on(
      'state_changed',
      (snapshot) => {
        const progress =
          (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
        updateProgress(fileId, progress);
      },
      (error) => console.error('Error uploading file:', error),
      async () => {
        const downloadURL = await getDownloadURL(uploadTask.snapshot.ref);
        const newFile = {
          id: fileId,
          path: storageRef.fullPath,
          url: downloadURL,
        };
        setFiles((prevFiles) => [...prevFiles, newFile]);
        setValue((prevFiles) => [...prevFiles, newFile]);
        setUploadProgress((prevProgress) =>
          prevProgress.filter((p) => p.id !== fileId)
        );

        setBytesTransferred(
          (prevBytes) => prevBytes + uploadTask.snapshot.bytesTransferred
        );
      }
    );
  }, []);

  const handleDelete = useCallback(
    async (fileId) => {
      const file = files.find((f) => f.id === fileId);
      if (!file) return;

      try {
        await deleteObject(ref(firebase.storage, file.path));
        const updatedFiles = files.filter((f) => f.id !== fileId);
        setFiles(updatedFiles);
        setValue(updatedFiles);
      } catch (error) {
        console.error('Error deleting file:', error);
      }
    },
    [files, setValue]
  );

  const progress =
    uploadProgress.reduce((acc, curr) => acc + curr.progress, 0) /
      uploadProgress.length || 0;

  return {
    files,
    handleUpload,
    handleDelete,
    progress,
    uploading,
    bytesTransferred,
    totalBytes,
    setTotalBytes,
  };
};

export default useCloudStorage;
