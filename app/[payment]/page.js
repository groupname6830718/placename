import ClientComponent from '@/app/[payment]/client-component';
import RootPage from '@/app/page.js';

export async function generateStaticParams() {
  const actionablePayments = await fetch(
    `https://www.units-server.com/api/payments?place=${encodeURIComponent(
      process.env.NEXT_PUBLIC_PLACE
    )}&status=requires_payment_method&keys=name`,
    { next: { tags: ['payments'] } }
  ).then((x) => x.json());

  return Object.entries(actionablePayments).map(([key, value]) => ({
    payment: key,
  }));
}
export default function Page({ params }) {
  return (
    <>
      <RootPage></RootPage>
      <ClientComponent params={params}></ClientComponent>
    </>
  );
}
