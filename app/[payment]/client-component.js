'use client';
import { useEffect } from 'react';

export default function ClientComponent({ params }) {
  useEffect(() => {
    const element = document.getElementById(params.payment);

    if (element) {
      element.classList.remove('invisible');
      element.classList.remove('-z-50');
    }
  }, [params.payment]);

  return <></>;
}
