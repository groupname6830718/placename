import Confetti from '@/app/_components/vacant/confetti';

export default async function RootLayout({ children }) {
  return (
    <>
      <Confetti></Confetti>
      {children}
    </>
  );
}
