require('dotenv').config();
const fs = require('fs');
const admin = require('firebase-admin');
const serviceAccount = {
  type: 'service_account',
  project_id: 'projectname-xxx',
  private_key_id: 'd8fca7995b859c4004c9c972745435e5a84ca8fd',
  private_key:
    '-----BEGIN PRIVATE KEY-----\nMIIEvAIBADANBgkqhkiG9w0BAQEFAASCBKYwggSiAgEAAoIBAQCy+J4D9rUBUdHk\nxpDvGfju6e17EwN4/N9ND/AP3EZORn2iUBK/Z3zni6uhN+dp9F27eudUPa/GHZiK\nenJ0JWzbsJNOqadk1onkKii5Ibjysvq8ZAse3KbCBz4qcuIR8LV2/U2FyhTUqH1p\ny/rLYx2Dy0Os2+9dbwj+SVYskmEdiIOOdbIEQl1iEeVItMD3yDFpyggD0pFOvorU\nksu23nm4glvoEa8FeXpWe/6o5v9tB0KGDYYBIzwLLqqBePcmXqse42/BbxEu7LIN\nMkI41PGyoaQVA9a0V+0tsPIfMvQuXnsFa4s3y7IbkJGuwGWQBSMqKuX3fOnuLTTY\nbhGxY+mHAgMBAAECggEAJF9Ahd+yo4UEWjE8rUsi+zMRb2zKzODfZOLzT9Xdd/T9\nfWo2lS2sBcat58Lbxf3CuuJcdxNeUK4dHS+LaS1d9TvqhBcYjPbI6VTkF9PRqYlU\nScsvM2N2hIJCURAt/6Viy77o7WLHXvyrGEA7RPBsxCcgJtY/zEZPlpZC7CYHlJ+d\nCGt0V59MtywkI8jbXq2BMbBbeaTMc+hwN17qM13pXAKc8W/cgW46PG/oD/JSXbMR\nt5q67KoPi7Jm1TmbqDmKItF/FPsk9siOZ/a5znYflplXP/BKWMZUvy1xEy982m4A\nyelskl/a8W5ttL4Ta+vLhS78153iyu2nYkKPQ59buQKBgQD1ByzT4EXZOwceMQSj\nSW/rAIkS6NKCleRxY3jckNSk6RkU5uqJ8u0vCyVcm4BOjlzM3yx+Lj65rHGK8zY8\nFtgHbnKRlsq0WRVYHuCyACuZtfYvFqcRwlz7zg1rVWI3Wmlk8UPnBkb+X+N0YR4j\nDxr+2eUTOK90//i/dx2lmncRiwKBgQC6/Da6XpPuG2C+5+y3OYE8mtoHE6cVTRpe\n8ofdiXvkV409eFgQAEFNLzJVtfCS7HBPHudJTQ82qh+KxpaEDehZrsuDr4xvGKlV\nWK7nP7eCJKtYNwO26bPME2MEYiDZPNjnkP0sc9whekLcKUDYp8s5/rDEONvnH79I\nijxsM41PdQKBgBKNQoWgO2mhIH7Ws0UxsDmYxsgZvKaKdstm/qWkiZUa2P1OJn/f\nVrnzHBcQ5vwLp1cTpqi/E5y3Q7mDhJ20FRMq952yTxKslAMiA102yhZPQp56Fpgv\noR6nkTiJNHCAde5gngWF4iYsWGpdWAzLff3Bvahg6FfxcYH9oM8DDPTVAoGAVqBm\nUm1ip1sztP4JotEXWJZ9D+5A6BNydi5yV2ZMaXtOSf4o/jDAEBwfe6nwrICxf1ZX\n3JPAypiCSG68aK1wyau7SWTY8NdCC/IsBaJ1R+vpHdNO+zzGOXN52hwED+CqtI1t\nXVzHzRKxSwIpsrX55wJhrmaRKkCBBABXAeHYQ8kCgYA3G6/1NrbdGrAN2YiBMR+s\n5XcUJncezHNuvPqLB0wroBxWGTMc2+10ZNp7AOindpUDlaFykCq4K6IwVS1Y5CsL\n/+n67T7O2p+j9+0NhkMIqYl/GPMkjlFbAiqcLspBIPFXs9PKeggGxQ4blZxIQw5i\nMfENaVcGIWgiRea9voOnUw==\n-----END PRIVATE KEY-----\n',
  client_email:
    'firebase-adminsdk-ds2dg@projectname-xxx.iam.gserviceaccount.com',
  client_id: '107866596398238878622',
  auth_uri: 'https://accounts.google.com/o/oauth2/auth',
  token_uri: 'https://oauth2.googleapis.com/token',
  auth_provider_x509_cert_url: 'https://www.googleapis.com/oauth2/v1/certs',
  client_x509_cert_url:
    'https://www.googleapis.com/robot/v1/metadata/x509/firebase-adminsdk-ds2dg%40projectname-xxx.iam.gserviceaccount.com',
};
admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
});
const db = admin.firestore();
const docRef = db.collection('places').doc(process.env.NEXT_PUBLIC_PLACE);

docRef
  .get()
  .then((doc) => {
    if (doc.exists) {
      const data = doc.data();

      const placeData =
        data.place === '2300 9th Ave SW UNIT A5 Olympia, WA 98502'
          ? {
              coordinates: data.coordinates,
              unit: data.unit,
              x: 0.00046658752874999997,
              y: 0.00028201219624999997,
              a: -0.000073512,
              b: 0.00004364775,
              d: -0.1424295,
              e: 0.0005127011739,
              f: 0.000927023076,
              g: 0.0011112276078,
              h: 0.000560308464,
              xx: true,
            }
          : {
              coordinates: data.coordinates,
              unit: data.unit,
              x: 0.00050441895,
              y: 0.00030487805,
              a: -0.00008,
              b: 0.0000475,
              d: -0.155,
              e: 0.000557951,
              f: 0.00100884,
              g: 0.001209302,
              h: 0.00060976,
              xx: false,
            };

      const filePath = 'public/map.js';

      fs.readFile(filePath, 'utf8', (err, content) => {
        if (err) {
          console.log('Error reading file:', err);
          return;
        }
        const lines = content.split('\n');
        lines.shift();
        const updatedContent = `const placeData = ${JSON.stringify(
          placeData
        )};\n${lines.join('\n')}`;
        fs.writeFile(filePath, updatedContent, (err) => {
          if (err) {
            console.log('Error writing file:', err);
          } else {
            console.log(
              `Successfully wrote map.js params const placeData = ${JSON.stringify(
                placeData
              )}`
            );
          }
        });
      });
    } else {
      console.log('No such document!');
    }
  })
  .catch((error) => {
    console.log('Error getting document:', error);
  });
